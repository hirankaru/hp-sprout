var onAPIReady = null;
var players = new Array();

$(document).ready(function() {
    // Where we set up the YouTube functions
    var scrolling = false;
    var targetRegion = null;
    var loadYouTube = function(playerSelector, videoId, autoplay){
        if($('#' + playerSelector + ':not(iframe)').length > 0){
            // Get the player object
            var selectedPlayer = $('#' + playerSelector + ':not(iframe)');

            // Select all parents with class bx-clone
            if(selectedPlayer.parents('.bx-clone').length > 0){
            // If parents exist with class, bail out
            return false;
        }
        if(typeof(YT) !== "undefined"){
            players.push(new YT.Player(
                playerSelector, {
                videoId: videoId,
                    playerVars: { 
                        'rel': 0
                    }
                }
            ));

        } else {
            onAPIReady = function(){ 
                loadYouTube(playerSelector, videoId, autoplay);
            } 
        }
        }
    };



	$("body").fitVids();
    var newIndex = -1;
    slideTravel(newIndex);
//Register Videos for Sprout Possibilities

	$('.module-2 .overlay-content a').click(function(e){
    	e.preventDefault();
        //var scrollPoss = $(document).scrollTop();
        //alert(scrollPoss);
        //$('body').addClass('fixed-body');
        $('body').css('overflow', 'hidden');
        var slideNum = $(this).attr('data-slide-index');

		// Get the Videos
    	var sellVid = $('#sell-trigger').data('video-id');
        var sellVidFull = $('#sell-trigger').data('video-id-full');
    	var teachVid = $('#teach-trigger').data('video-id');
        var teachVidFull = $('#teach-trigger').data('video-id-full');

    	
    	// Get the Players
		var sellPlayer = $('#sell-trigger').data('video-container');
        var sellPlayerFull = $('#sell-trigger').data('video-container-full');
    	var teachPlayer	= $('#teach-trigger').data('video-container');
        var teachPlayerFull	= $('#teach-trigger').data('video-container-full');

    	
    	// Load the Videos
    	loadYouTube(sellPlayer, sellVid);
        loadYouTube(sellPlayerFull, sellVidFull);
    	loadYouTube(teachPlayer, teachVid);
        loadYouTube(teachPlayerFull, teachVidFull);

        $('div[id=possibility-player-7]').html($('iframe[id=possibility-player-7]'));
    	
    	// Run videos through FitVids
    	$("body").fitVids();
    	
    	// Show the Overlay
		$('#possibilities-cover').show();
		
		// Click outside the container closes the overlay
		//BindOutsideTargetTracking('#possibility-overlay li .container, #possibilities-cover .bx-controls.bx-has-controls-direction', function(){
			//$('#possibility-close').click();
		//});
		//window.possSliderWrapper.forceResize();

        if($('#possibilities-cover').is(':visible')){
            $('#possibilities-cover').css({'background-color':'#fff'});
            initSliderOverlay ();
        }

        $(window).resize(function(){
            initSliderOverlay ();
        });

        //window.possSliderWrapper.goToSlide(slideNum);
        window.possSliderWrapper.goToSlide(slideNum);

    });

    //module 2 mobile video
    /*$('.module-2 .videoHolder .fitHolder a.mobileOnly').not('.module-2 .videoHolder .fitHolder a.secondary').click(function(e){
        e.preventDefault();

        var sellVidRM2 = $(this).data('video-id');
        var sellPlayerRM2 = $(this).data('video-container');
        if($(this).hasClass('primary')) {
            $(this).parent().parent().parent().find('.imageHolder img').hide();
        }else{
            $(this).parent().parent().find('.imageHolder img').hide();
        }

        loadYouTube(sellPlayerRM2, sellVidRM2);
        // Run videos through FitVids
        $("body").fitVids();
    });*/


    //module-5 overlay slider
    $('.module-5 .artistVideoGallery .hoverBoxContainer .more a').click(function(e){
        //var scrollPos = $(window).scrollTop();
        //alert(scrollPos);



        e.preventDefault();
        //$('body').addClass('fixed-body');
        $('body').css('overflow', 'hidden');
        var slideNum = $(this).attr('data-slide-index');
        // Get the Videos
        var sellVid1 = $('#module-5-trigger-1-1').data('video-id');
        var teachVid1 = $('#module-5-trigger-1-2').data('video-id');
        var doVid1 = $('#module-5-trigger-1-3').data('video-id');
        var presentVid1 = $('#module-5-trigger-1-4').data('video-id');
        //alert(presentVid1);
        // Get the Players
        var sellPlayer1 = $('#module-5-trigger-1-1').data('video-container');
        var teachPlayer1	= $('#module-5-trigger-1-2').data('video-container');
        var doPlayer1 = $('#module-5-trigger-1-3').data('video-container');
        var presentPlayer1 = $('#module-5-trigger-1-4').data('video-container');
        //alert(presentPlayer1);
        // Load the Videos
        loadYouTube(sellPlayer1, sellVid1);
        loadYouTube(teachPlayer1, teachVid1);
        loadYouTube(doPlayer1, doVid1);
        loadYouTube(presentPlayer1, presentVid1);

        $('div[id=module-5-player-7]').html($('iframe[id=module-5-player-7]'));

        // Run videos through FitVids
        $("body").fitVids();

        // Show the Overlay
        $('#module-5-cover').show();

        // Click outside the container closes the overlay
        //BindOutsideTargetTracking('#possibility-overlay li .container, #possibilities-cover .bx-controls.bx-has-controls-direction', function(){
        //$('#possibility-close').click();
        //});
        //window.possSliderWrapper.forceResize();

        if($('#module-5-cover').is(':visible')){
            $('#module-5-cover').css({'background-color':'#fff'});
            initSliderOverlay ();
        }

        $(window).resize(function(){
            initSliderOverlay ();
        });

        $('#slider').addClass('instant-slide');
        setTimeout(function(){
            $('#slider').removeClass('instant-slide');
        }, 100);

        window.module5SliderWrapper.goToSlide(slideNum);

    });


    //module-5 overlay slider 2
    // TEST BRANCH
    $('.module-5 .vignetteVideos .hoverBoxContainer .more a').click(function(e){

        e.preventDefault();
        //$('body').addClass('fixed-body');
        $('body').css('overflow', 'hidden');
        var slideNum = $(this).attr('data-slide-index');
        // Get the Videos
        var sellVid2 = $('#module-5-trigger-3-1').data('video-id');
        //alert(sellVid2);
        var teachVid2 = $('#module-5-trigger-3-2').data('video-id');
        var doVid2 = $('#module-5-trigger-3-3').data('video-id');
        var presentVid2 = $('#module-5-trigger-3-4').data('video-id');

        // Get the Players
        var sellPlayer2 = $('#module-5-trigger-3-1').data('video-container');
        var teachPlayer2	= $('#module-5-trigger-3-2').data('video-container');
        var doPlayer2 = $('#module-5-trigger-3-3').data('video-container');
        var presentPlayer2 = $('#module-5-trigger-3-4').data('video-container');


        // Load the Videos
        loadYouTube(sellPlayer2, sellVid2);
        loadYouTube(teachPlayer2, teachVid2);
        loadYouTube(doPlayer2, doVid2);
        loadYouTube(presentPlayer2, presentVid2);

        $('div[id=module-5-player-7]').html($('iframe[id=module-5-player-7]'));

        // Run videos through FitVids
        $("body").fitVids();

        // Show the Overlay
        $('#module-5-cover-2').show();

        // Click outside the container closes the overlay
        //BindOutsideTargetTracking('#possibility-overlay li .container, #possibilities-cover .bx-controls.bx-has-controls-direction', function(){
        //$('#possibility-close').click();
        //});
        //window.possSliderWrapper.forceResize();

        if($('#module-5-cover-2').is(':visible')){
            $('#module-5-cover-2').css({'background-color':'#fff'});
            initSliderOverlay ();
        }

        $(window).resize(function(){
            initSliderOverlay ();
        });
        window.module5SliderWrapperTwo.goToSlide(slideNum);

    });

    //module 5 mobile video
    $('.module-5 .tabs-panels .slide .thumbs li .hoverBoxContainer .play.mobileOnly').not('.module-5 .tabs-panels .slide .thumbs li .hoverBoxContainer .play.secondary').click(function(e){
        e.preventDefault();
        var sellVidRM = $(this).data('video-id');
        var sellPlayerRM = $(this).data('video-container');
        loadYouTube(sellPlayerRM, sellVidRM);
        // Run videos through FitVids
        $("body").fitVids();
    });

    //module 5 gallery mobile video
    /*$('.module-5-5 .hoverBoxContainer .play.mobileOnly').click(function(e){
        e.preventDefault();
        var sellVidRM5 = $(this).data('video-id');
        var sellPlayerRM5 = $(this).data('video-container');
        loadYouTube(sellPlayerRM5, sellVidRM5);
        // Run videos through FitVids
        $(this).parent().parent().hide();
        $("body").fitVids();
    });*/

    //module 5 gallery mobile video
    /*$('.module-5-5-2 .hoverBoxContainer .play.mobileOnly').click(function(e){
        e.preventDefault();
        var sellVidRM5 = $(this).data('video-id');
        var sellPlayerRM5 = $(this).data('video-container');
        loadYouTube(sellPlayerRM5, sellVidRM5);
        // Run videos through FitVids
        $(this).parent().parent().hide();
        $("body").fitVids();
    });*/

    // resources overlay slider
    $('.module-21 .container .overlay-content a').click(function(e){
        //var scrollPos = $(window).scrollTop();
        //alert(scrollPos);



        e.preventDefault();
        //$('body').addClass('fixed-body');
        $('body').css('overflow', 'hidden');
        var slideNum = $(this).attr('data-slide-index');
        // Get the Videos
        var sellVidR = $('#module-22-trigger-1-1').data('video-id');
        var teachVidR = $('#module-22-trigger-1-2').data('video-id');
        var doVidR = $('#module-22-trigger-1-3').data('video-id');
        var presentVidR = $('#module-22-trigger-1-4').data('video-id');
        //alert(presentVid1);
        // Get the Players
        var sellPlayerR = $('#module-22-trigger-1-1').data('video-container');
        var teachPlayerR	= $('#module-22-trigger-1-2').data('video-container');
        var doPlayerR = $('#module-22-trigger-1-3').data('video-container');
        var presentPlayerR = $('#module-22-trigger-1-4').data('video-container');
        //alert(presentPlayer1);
        // Load the Videos
        loadYouTube(sellPlayerR, sellVidR);
        loadYouTube(teachPlayerR, teachVidR);
        loadYouTube(doPlayerR, doVidR);
        loadYouTube(presentPlayerR, presentVidR);

        $('div[id=module-5-player-7]').html($('iframe[id=module-5-player-7]'));

        // Run videos through FitVids
        $("body").fitVids();

        // Show the Overlay
        $('#resource-cover').show();

        // Click outside the container closes the overlay
        //BindOutsideTargetTracking('#possibility-overlay li .container, #possibilities-cover .bx-controls.bx-has-controls-direction', function(){
        //$('#possibility-close').click();
        //});
        //window.possSliderWrapper.forceResize();

        if($('#resource-cover').is(':visible')){
            $('#resource-cover').css({'background-color':'#fff'});
            initSliderOverlay ();
        }

        $(window).resize(function(){
            initSliderOverlay ();
        });
        window.module22Slider.goToSlide(slideNum);

    });


    //module 21 mobile video
    /*$('.module-21 .container .mobileVideoPlay a').click(function(e){
        e.preventDefault();
        var sellVidRM21 = $(this).data('video-id');
        var sellPlayerRM21 = $(this).data('video-container');
        loadYouTube(sellPlayerRM21, sellVidRM21);
        $(this).parent().parent().find('img').hide();
        $(this).hide();
        // Run videos through FitVids
        $("body").fitVids();
    });*/


    function initSliderOverlay (){
        var overlayContentHeight = $('.slider-overlay ul li .container').height();
        var overlaySlidePosition = (overlayContentHeight/2)+10;
        var sliderBoxWidth = $('.slider-overlay ul li .container .cover-overlay-content iframe').outerWidth();
        var SliderHeight = overlayContentHeight;
        $('.slider-overlay .bx-viewport').css({'height': SliderHeight-95});
        $('.slider-overlay .bx-controls').css({'top': overlaySlidePosition});
        $('.slider-overlay .thumb-links').width(sliderBoxWidth+15);
    }


//Register videos for Sprout Stories

	$('#story-thumbs .overlay-content a').click(function(e){
    	e.preventDefault();
		
		// Get the Videos
    	var joshVid = $('#josh-trigger').data('video-id');
    	var toddVid = $('#todd-trigger').data('video-id');
    	var erinVid = $('#erin-trigger').data('video-id');
    	
    	// Get the Players
		var joshPlayer = $('#josh-trigger').data('video-container');
    	var toddPlayer	= $('#todd-trigger').data('video-container');
    	var erinPlayer = $('#erin-trigger').data('video-container');   	
    	
    	// Load the Videos
    	loadYouTube(joshPlayer, joshVid);
    	loadYouTube(toddPlayer, toddVid);
    	loadYouTube(erinPlayer, erinVid);

        $('div[id=story-player-3]').html($('iframe[id=story-player-3]'));
    	
    	// Run videos through FitVids
    	$("body").fitVids();
    	
    	// Show the Overlay
		$('#stories-cover').show();
		
		// Click outside the container closes the overlay
		BindOutsideTargetTracking('#story-overlay li .container, #stories-cover .bx-controls.bx-has-controls-direction', function(){
            $('#story-close').click();
        });	
		window.storySliderWrapper.forceResize();
    });




// Register videos for Sprout Support

    $('#tutorials-thumbs .overlay-content a').click(function(e){
        e.preventDefault();
        
        // Get the Videos
        var tipsVid = $('#tips-trigger').data('video-id');
        var cropVid = $('#crop-trigger').data('video-id');
        var captVid = $('#capture-trigger').data('video-id');
        var textVid = $('#text-trigger').data('video-id');
        
        // Get the Players
        var tipsPlayer = $('#tips-trigger').data('video-container');
        var cropPlayer  = $('#crop-trigger').data('video-container');
        var captPlayer = $('#capture-trigger').data('video-container');    
        var textPlayer = $('#text-trigger').data('video-container');    
        
        // Load the Videos
        loadYouTube(tipsPlayer, tipsVid);
        loadYouTube(cropPlayer, cropVid);
        loadYouTube(captPlayer, captVid);
        loadYouTube(textPlayer, textVid);

        $('div[id=tutorial-player-4]').html($('iframe[id=tutorial-player-4]'));
        
        // Run videos through FitVids
        $("body").fitVids();
        
        // Show the Overlay
        $('#tutorials-cover').show();
        
        // Click outside the container closes the overlay
        BindOutsideTargetTracking('#tutorials-overlay li .container, #tutorials-cover .bx-controls.bx-has-controls-direction', function(){
            $('#tutorials-close').click();
        });     
        window.tipsSliderWrapper.forceResize();
    });

//Register videos for generic video buttons

	$('.video-trigger').click(function(e) {
		e.preventDefault();
		var videoSource = $(this).data('video-id');
		var videoTarget = $(this).data('video-target');
		var overlay = $(this).data('overlay');

		loadYouTube(videoTarget, videoSource);
		$('body').fitVids();
		$('#' + overlay).fadeIn(500, function(){
			BindOutsideTargetTracking('.full-overlay-content', function(){
				$('.close-button').click();
			});
		});
	});

// Mobile version of above

	$('.mobile-video-trigger').click(function(e) {
        stopAll();
		e.preventDefault();
		var videoSource = $(this).data('mobile-video');
		var videoTarget = $(this).data('player');
		$(this).hide();
		loadYouTube(videoTarget, videoSource);

		$('body').fitVids();
	});

    function slideTravel(newIndex){
        $('.slider-overlay .thumb-links').each(function(){
                $(this).find('.img-wrap:first .now-play').show();
        });
        $('.slider-overlay .thumb-links').each(function(){
            $(this).find('.img-wrap').click(function(){
                var SlideNum = $(this).attr('data-slide-index');
                if($('#possibilities-cover').is(':visible')) {
                    window.possSliderWrapper.goToSlide(SlideNum);
                }
                if($('#module-5-cover').is(':visible')) {
                    window.module5SliderWrapper.goToSlide(SlideNum);
                }
                if($('#module-5-cover-2').is(':visible')) {
                    window.module5SliderWrapperTwo.goToSlide(SlideNum);
                }
                if($('#resource-cover').is(':visible')) {
                    window.module22Slider.goToSlide(SlideNum);
                }
                $('.slider-overlay .thumb-links .img-wrap .now-play').hide();
                $(this).find('.now-play').show();
            })
        });
    }

});
