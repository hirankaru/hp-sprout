var globals = {
	lastWindowHeight: $(window).height(),
	lastWindowWidth: $(window).width()
}
var resizeHandler = function () {
	// has the window actually resized?
	// some browsers fire the resize event more than they have to
	if ($(window).height() != globals.lastWindowHeight || $(window).width() != globals.lastWindowWidth) {
		globals.lastWindowHeight = $(window).height();
		globals.lastWindowWidth = $(window).width();
	}
}

$(document).ready(function() {

    //$('.slider-overlay ul li').css('height', $(document).height());

	$('.close-button').click(function(e) {
		$('.full-overlay:visible').hide();
		$('#details').height('auto');
		e.preventDefault();
	});

	$('.meet-3d-trigger').click(function (e) {
		e.preventDefault();
		$('#meet-3d-overlay').fadeIn(500, function(){
			BindOutsideTargetTracking('.container', function(){
				$('#meet-3d-close').click();
			});	
		});
		$('#meet-3d-overlay iframe').attr("src", "/360/index.html");
	});

	$('.spec-trigger').click(function (e) {
		e.preventDefault();
		$('#spec-overlay').fadeIn(500, function(){
			BindOutsideTargetTracking('.container', function(){
				$('.close-button').click();
			});	
		});
		
		if($('#spec-overlay .container').height() >= globals.lastWindowHeight) {
			var sectionPos = $(".sprout-closer").position();
			var overlayHeight = $('#spec-overlay .container').height() + 100;
			var overlayAbsPos = sectionPos.top;
			$("#spec-overlay").css({ "position": "absolute", "height": overlayHeight, "top": overlayAbsPos });
		} else {
			$("#spec-overlay").css({ "position": "fixed", "height": overlayHeight, "top": "0" });
		}
		
	});

	$('#details .mobile-specs a').click(function (e) {
		var h = $('#details .vertical-centre').height() + 30;
		$('#details').height(h);
	});	

	$('#signup-trigger').click(function(e) {
		e.preventDefault();
		$('#signup-overlay').fadeIn(500, function(){
			BindOutsideTargetTracking('.container', function(){
				$('.close-button').click();
			});	
		});

		if($('#signup-overlay .container').height() >= globals.lastWindowHeight) {
			var sectionPos = $(".sprout-more").position();
			var overlayHeight = $('#signup-overlay .container').height() + 100;
			var overlayAbsPos = sectionPos.top;
			$("#signup-overlay").css({ "position": "absolute", "height": overlayHeight, "top": overlayAbsPos });
		} else {
			$("#signup-overlay").css({ "position": "fixed", "height": overlayHeight, "top": "0" });
		}
		
	});
});