$(document).ready(function() {

//Register Possibility Overlay Slider

	$('#possibilities-cover').show();
	var possSlider;
	window.possSliderWrapper = $('#possibility-overlay').bxSlider({
		pagerCustom: '#possibility-thumbs',
        useCSS: false,
		speed: 0,
		infiniteLoop: true,
		hideControlOnEnd: false,
		onSliderLoad: function(){
			possSlider = this;
			$('#possibilities-cover').hide();
  	},
       onSlideAfter: function($slideElement, oldIndex, newIndex){
			stopAll();
			this.speed = 500;	
			
			// Update red border on the thumbnails
			$('#possibility-overlay li').removeClass('active');
			
			var newSlide = $('#possibility-overlay li')[newIndex];
			$(newSlide).addClass('active');

			var firstVisibleSlide = $('#possibility-overlay li:not(.hidden)').first();

			//if (firstVisibleSlide[0] == newSlide) {
			  //  $('#possibilities-cover .bx-prev').hide();
			//} else {
			  //  $('#possibilities-cover .bx-prev').show();
			//}

            $('.slider-overlay .thumb-links .img-wrap .now-play').hide();
            $('.slider-overlay .thumb-links').each(function(){
                $(this).find('.img-wrap#' + newIndex + ' .now-play').show();
            });
		}
	});

	/*$('.close-button.poss').click(function(e) {
		e.preventDefault();
		possSlider.speed = 0;
        $('#possibilities-cover').hide();
        //$('body').removeClass('fixed-body');
        $('body').css('overflow', 'visible');
    });*/


    /*$('.thumb-links a').click(function(e) {
        //var slider = $('#possibility-overlay').bxSlider();
        window.possSliderWrapper.goToSlide(1);

    });*/

// Possibility Mobile Slider Settings

	$('#possibilities-mobile-slider').bxSlider({
    	controls: false,
    	onSlideAfter: function($slideElement, oldIndex, newIndex){
			stopAll();
		}
    });
    $('#possibilities-mobile-slider').find('.possibility-mobile-player').each( function(i){
      var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
      $(this).attr('id', 'possibility-mobile-player-' + postnum);
    });
	$('#possibilities-mobile-slider').find('.mobile-video-trigger').each( function(i){
      var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
      $(this).attr('data-player', 'possibility-mobile-player-' + postnum);
    });
    $('.cover-overlay-content').find('.possibility-video-player').each( function(i){
      var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
      $(this).attr('id', 'possibility-player-2-' + postnum);
    });

//Register module 5 Overlay Slider 1

 $('#module-5-cover').show();
    var module5Slider;
    window.module5SliderWrapper = $('#module-5-overlay').bxSlider({
        pagerCustom: '#story-thumbs',
        useCSS: false,
        speed: 0,
        infiniteLoop: true,
        hideControlOnEnd: false,
        onSliderLoad: function(){
            module5Slider = this;
            $('#module-5-cover').hide();
        },
        onSlideAfter: function($slideElement, oldIndex, newIndex){
            stopAll();
            this.speed = 500;
            //alert(newIndex);
            $('#module-5-overlay li').removeClass('active');

            var newSlide = $('#module-5-overlay li')[newIndex];
            $(newSlide).addClass('active');

            //var firstVisibleSlide = $('#stories-cover li:not(.hidden)').first();
            //alert(newIndex);
            $('.slider-overlay .thumb-links .img-wrap .now-play').hide();
            $('.slider-overlay .thumb-links').each(function(){
                $(this).find('.img-wrap#' + newIndex + ' .now-play').show();
            });
            //$('.img-wrap#' + newIndex).find('.now-play').show();

            //if (firstVisibleSlide[0] == newSlide) {
            //  $('#stories-cover .bx-prev').hide();
            //} else {
            //  $('#stories-cover .bx-prev').show();
            //}
        }
    });

    $('.cover-overlay-content').find('.module-5-video-player').each( function(i){
        var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
        $(this).attr('id', 'module-5-player-' + postnum);
    });

    /*$('.module-5-close').click(function(e) {
        //alert('dfgdfg');
        e.preventDefault();
        module5Slider.speed = 0;
        $('#module-5-cover, #module-5-cover-2').hide();
        $('html, body').animate({
            scrollTop: $(".footer").offset().top
        }, 200);
    });*/

//Register module 5 Overlay Slider 2

    $('#module-5-cover-2').show();
    var module5SliderTwo;
    window.module5SliderWrapperTwo = $('#module-5-overlay-2').bxSlider({
        pagerCustom: '#story-thumbs',
        useCSS: false,
        speed: 0,
        infiniteLoop: true,
        hideControlOnEnd: false,
        onSliderLoad: function(){
            module5SliderTwo = this;
            $('#module-5-cover-2').hide();
        },
        onSlideAfter: function($slideElement, oldIndex, newIndex){
            stopAll();
            this.speed = 500;

            $('#module-5-overlay li').removeClass('active');

            var newSlide = $('#module-5-overlay li')[newIndex];
            $(newSlide).addClass('active');

            var firstVisibleSlide = $('#stories-cover li:not(.hidden)').first();
            //alert(newIndex);
            $('.slider-overlay .thumb-links .img-wrap .now-play').hide();
            $('.slider-overlay .thumb-links').each(function(){
                $(this).find('.img-wrap#' + newIndex + ' .now-play').show();
            });
            //$('.img-wrap#' + newIndex).find('.now-play').show();

            //if (firstVisibleSlide[0] == newSlide) {
            //  $('#stories-cover .bx-prev').hide();
            //} else {
            //  $('#stories-cover .bx-prev').show();
            //}
        }
    });

    $('#module-5-overlay-2 .cover-overlay-content').find('.module-5-video-player-2').each( function(i){
        var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
        $(this).attr('id', 'module-5-player-2-' + postnum);
    });

    $('#resource-cover').show();
        var resourceSliderTwo;
        window.module22Slider = $('#resource-overlay').bxSlider({
        pagerCustom: '#story-thumbs',
        useCSS: false,
        speed: 0,
        infiniteLoop: true,
        hideControlOnEnd: false,
        onSliderLoad: function(){
            resourceSliderTwo = this;
            $('#resource-cover').hide();
        },
        onSlideAfter: function($slideElement, oldIndex, newIndex){
            stopAll();
            this.speed = 500;

            $('#module-5-overlay li').removeClass('active');

            var newSlide = $('#module-5-overlay li')[newIndex];
            $(newSlide).addClass('active');

            var firstVisibleSlide = $('#stories-cover li:not(.hidden)').first();
            //alert(newIndex);
            $('.slider-overlay .thumb-links .img-wrap .now-play').hide();
            $('.slider-overlay .thumb-links').each(function(){
                $(this).find('.img-wrap#' + newIndex + ' .now-play').show();
            });
            //$('.img-wrap#' + newIndex).find('.now-play').show();

            //if (firstVisibleSlide[0] == newSlide) {
            //  $('#stories-cover .bx-prev').hide();
            //} else {
            //  $('#stories-cover .bx-prev').show();
            //}
        }
    });

    $('#resource-cover .cover-overlay-content').find('.resource-video-player').each( function(i){
        var postnum = i + 1; // remove any possible overloaded "+" operator ambiguity
        $(this).attr('id', 'module-22-player-' + postnum);
    });

    slideClose();
    
    /*$('.module-22-close').click(function(e) {
        //alert('dfgdfg');
        e.preventDefault();
        module22Slider.speed = 0;
        $('#resource-cover').hide();
    });*/


    //common function for overlay close
    function slideClose(){
        $('.slider-overlay .close-button').click(function(){
            if($('.module-21').is(':visible')){
                resourceSliderTwo.speed = 0;
            }
            $(this).parents().find('.slider-overlay').hide();
            $('body').css('overflow', 'visible');
            possSlider.speed = 0;
            module5Slider.speed = 0;
            module5SliderTwo.speed = 0;


        });
    }



});
