
// Testing overlays

$('#test').click(function (e) {
    e.preventDefault();
    $('#intro-video').fadeIn(500, function(){
        BindOutsideTargetTracking('.container', function(){
            $('#meet-3d-close').click();
        });
    });
    $('#meet-3d-overlay iframe').attr("src", "/360/index.html");
});

$('#test2').click(function (e) {
    e.preventDefault();
    $('#intro-video2').fadeIn(500, function(){
        BindOutsideTargetTracking('.container', function(){
            //$('#meet-3d-close').click();
        });
    });
    $('#meet-3d-overlay iframe').attr("src", "/360/index.html");
});